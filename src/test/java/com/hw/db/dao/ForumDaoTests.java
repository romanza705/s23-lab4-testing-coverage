package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.mockito.Mockito.*;

class ForumDaoTests {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forumDao = new ForumDAO(mockJdbc);
    UserDAO.UserMapper userMapper = new UserDAO.UserMapper();
    @Test
    void ForumDaoTest1() {
        ForumDAO.UserList("mock", 7, "10.10.1095", true);
        verify(mockJdbc).query
                (Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                        Mockito.any(Object[].class),
                        Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest2() {
        ForumDAO.UserList("mock", 7, "10.10.1095", false);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest3() {
        ForumDAO.UserList("mock", null, "10.10.1095", false);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest4() {
        ForumDAO.UserList("mock", null, "10.10.1095", true);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest5() {
        ForumDAO.UserList("mock", 7, null, false);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest6() {
        ForumDAO.UserList("mock", 7, null, true);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest7() {
        ForumDAO.UserList("mock", null, null, true);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void ForumDaoTest8() {
        ForumDAO.UserList("mock", null, null, false);
        verify(mockJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                        "WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }
}