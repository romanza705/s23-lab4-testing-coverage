package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Random;
import static org.mockito.Mockito.*;

class PostDaoTests {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    PostDAO postDAO = new PostDAO(jdbcMock);
    Random random = new Random();
    Post existingPost;
    @BeforeEach
    void setUp() {
        this.existingPost = getPost(
                RandomString.make(7),
                RandomString.make(7),
                Timestamp.valueOf(LocalDateTime.now().plusHours(random.nextInt(7))),
                random.nextInt(7));

        Mockito.when(jdbcMock.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.any()))
                .thenReturn(existingPost);
    }

    private Post getPost(String author,
                         String message,
                         Timestamp created,
                         Integer id) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(created);
        post.setId(id);
        return post;
    }

    @Test
    void PostDaoTest1() {
        Post postTemp = getPost(
                existingPost.getAuthor(),
                existingPost.getMessage(),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Mockito.any(Object[].class));
    }
    @Test
    void PostDaoTest2() {
        Post postTemp = getPost(
                RandomString.make(7),
                existingPost.getMessage(),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    void PostDaoTest3() {
        Post postTemp = getPost(
                existingPost.getAuthor(),
                RandomString.make(7),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    void PostDaoTest4() {
        Post postTemp = getPost(
                existingPost.getAuthor(),
                existingPost.getMessage(),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(7))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void PostDaoTest5() {
        Post postTemp = getPost(
                RandomString.make(7),
                RandomString.make(7),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    void PostDaoTest6() {
        Post postTemp = getPost(
                RandomString.make(7),
                RandomString.make(7),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(10))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void PostDaoTest7() {
        Post postTemp = getPost(
                existingPost.getAuthor(),
                RandomString.make(7),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(10))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void PostDaoTest8() {
        Post postTemp = getPost(
                RandomString.make(7),
                existingPost.getMessage(),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(7))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), postTemp);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }




}