package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

class ThreadDaoTests {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ThreadDAO thread = new ThreadDAO(mockJdbc);
    @Test
    void ThreadDaoTest1() {
        ThreadDAO.treeSort(49, 14, 7, null);
        verify(mockJdbc)
                .query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  " +
                                "AND branch > (SELECT branch  FROM posts WHERE id = ?)  " +
                                "ORDER BY branch LIMIT ? ;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.any());


    }

    @Test
    void ThreadDaoTest2() {
        ThreadDAO.treeSort(49, 14, 7, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  " +
                        "AND branch < (SELECT branch  FROM posts WHERE id = ?)  " +
                        "ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }

    @Test
    void ThreadDaoTest3() {
        ThreadDAO.treeSort(49, 14, 7, false);
        verify(mockJdbc)
                .query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  " +
                                "AND branch > (SELECT branch  FROM posts WHERE id = ?)  " +
                                "ORDER BY branch LIMIT ? ;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.any());
    }

}