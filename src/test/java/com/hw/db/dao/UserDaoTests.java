package com.hw.db.dao;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.Optional;

import static org.mockito.Mockito.*;

class UserDaoTests {

    @Test
    void UserDaoUpdTest1() {
        User user = new User("romanza", "example@email.com", "name", "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    void UserDaoUpdTest2() {
        User user = new User("romanza", null, "name", "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    void UserDaoUpdTest3() {
        User user = new User("romanza", "example@email.com", null, "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    void UserDaoUpdTest4() {
        User user = new User("romanza", "example@email.com", "name", null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    void UserDaoUpdTest5() {
        User user = new User("romanza", null, null, "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    void UserDaoUpdTest6() {
        User user = new User("romanza", "example@email.com", null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }

    @Test
    void UserDaoUpdTest7() {
        User user = new User("romanza", null, "fullname", null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Optional.ofNullable(Mockito.any()));
    }
}